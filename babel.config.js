const presets = [
  '@babel/preset-typescript',
  '@babel/preset-react',
  [
    '@babel/preset-env',
    {
      useBuiltIns: 'usage',
      corejs: 3,
    },
  ],
];

const plugins = [
  'react-hot-loader/babel',
  ['@babel/plugin-transform-runtime', { 'corejs': 3 }],
  ['@babel/plugin-proposal-class-properties'],
];

module.exports = { presets, plugins };
