import React, { useEffect } from 'react';
import { Dispatch } from 'redux';
import { useDispatch, useSelector } from 'react-redux';

import { RootState } from '../../modules/store/types';
import { SideBarAction, CloseSideBarAction } from '../../modules/store/sideBar/types';
import * as actions from '../../modules/store/sideBar/actions';

import Row from '../Grid/Row';
import Content from '../Content';

import Header from '../Header';
import SideBar from '../SideBar';

import styles from './styles/Layout.scss';

type LayoutProps = {
  children: React.ReactNode;
};

const Layout: React.FC<LayoutProps> = ({ children }: LayoutProps) => {
  const dispatch = useDispatch<Dispatch<SideBarAction>>();

  const postId = useSelector<RootState, string | null>(state => state.post.id);
  const showSideBar = useSelector<RootState, boolean>(state => state.sideBar.display);

  const closeSideBar = (): CloseSideBarAction => dispatch(actions.closeSideBar());

  useEffect(() => {
    closeSideBar();
  }, [postId]);

  const handleSideBarClose = (): void => {
    closeSideBar();
  };

  const handleSideBarOpen = (): void => {
    dispatch(actions.openSideBar());
  };

  return (
    <div className={styles.layout}>
      <Row>
        <Header onSideBarOpen={handleSideBarOpen} />
        <SideBar
          sm
          show={showSideBar}
          onSideBarClose={handleSideBarClose}
        />
      </Row>
      <Row className={styles.main}>
        <SideBar show md lg />
        <Content>
          {children}
        </Content>
      </Row>
    </div>
  );
};

export default Layout;
