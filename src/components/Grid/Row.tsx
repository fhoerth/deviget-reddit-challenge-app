import React from 'react';
import classnames from 'classnames';

import styles from './styles/Row.scss';

type RowProps = {
  children: React.ReactNode;
  className?: string;
};

const Row: React.SFC<RowProps> = ({ children, className }: RowProps) => (
  <div className={classnames(styles.row, className)}>
    {children}
  </div>
);

export default Row;
