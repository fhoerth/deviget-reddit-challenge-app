// eslint-disable-next-line import/prefer-default-export
export const slugifyPostTitle = (permaLink: string): string => {
  const urlParts = permaLink.replace(/\/$/, '').split('/');

  return urlParts[urlParts.length - 1];
};
