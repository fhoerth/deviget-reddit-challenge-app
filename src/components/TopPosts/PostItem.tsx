import React, { useState, useEffect } from 'react';
import classnames from 'classnames';
import distanceInWords from 'date-fns/distance_in_words';
import { Link } from 'react-router-dom';

import { Post } from '../../modules/reddit/types';
import { getPostImageUrl } from '../../modules/common/utils';

import { slugifyPostTitle } from './utils';

import styles from './styles/PostItem.scss';

type PostItemProps = {
  post: Post;
  visited: boolean;
  dismissed: boolean;
  onDismiss: (post: Post) => void;
};

const animationDuration = 500;

const PostItem: React.FC<PostItemProps> = ({
  post,
  dismissed,
  visited,
  onDismiss,
}: PostItemProps) => {
  const [display, setDisplay] = useState<boolean>(true);

  useEffect(() => {
    if (dismissed) {
      setTimeout(() => {
        setDisplay(false);
      }, animationDuration);
    }
  }, [dismissed]);

  if (!display) {
    return null;
  }

  const {
    author,
    title,
    num_comments: numComments,
    created_utc: createdUtc,
  } = post;
  const handleDismiss = () => onDismiss(post);
  const link = `/post/${post.id}/${slugifyPostTitle(post.permalink)}`;

  const previewImage = post && post.preview && post.preview.images[0];
  const firstResolutionThumbnail = previewImage ? previewImage.resolutions[0] : null;
  const thumbnail = firstResolutionThumbnail ? (
    <img
      alt={title}
      src={getPostImageUrl(firstResolutionThumbnail)}
      width={firstResolutionThumbnail.width}
      height={firstResolutionThumbnail.height}
    />
  ) : null;

  return (
    <article className={classnames(styles.postItem, dismissed ? styles.postItemDismissed : null)}>
      <div className={styles.content}>
        <div className={styles.row}>
          <Link className={styles.author} to={link}>
            {!visited && <span className={styles.circle} />}
            {author}
          </Link>
          <span className={styles.createdAt}>
            {distanceInWords(createdUtc * 1000, new Date())}
            {' '}
            ago.
          </span>
        </div>
        <div className={styles.row}>
          <Link className={styles.title} to={link}>
            {thumbnail}
            {title}
          </Link>
        </div>
        <div className={styles.row}>
          <div className={styles.dismissPost} onClick={handleDismiss}>
            <span className={styles.closeIcon} />
            <span>
              Dismiss Post
            </span>
          </div>
          <span className={styles.comments}>
            {numComments}
            {' '}
            comments.
          </span>
        </div>
      </div>
    </article>
  );
};

export default PostItem;
