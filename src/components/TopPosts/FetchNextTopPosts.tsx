import React from 'react';
import classnames from 'classnames';

import config from '../../modules/reddit/config';

import styles from './styles/FetchNextTopPosts.scss';

type FetchNextTopPostsProps = {
  animate: boolean;
  onFetchNextTopPosts: () => void;
};

const FetchNextTopPosts: React.FC<FetchNextTopPostsProps> = ({
  animate,
  onFetchNextTopPosts,
}: FetchNextTopPostsProps) => (
  <div
    className={classnames(
      styles.fetchNextTopPosts,
      animate ? styles.fetchNextTopPostsAnimate : null,
    )}
  >
    All posts are dismissed.
    <br />
    <button type="button" className={styles.button} onClick={onFetchNextTopPosts}>
      Get next TOP
      {' '}
      {config.topPosts.limit}
      {' '}
      posts.
    </button>
  </div>
);

export default FetchNextTopPosts;
