import React, { useState } from 'react';
import { Post } from '../../modules/reddit/types';

import { isPostVisited, isPostDismissed } from '../../modules/common/utils';
import { DismissedPosts, VisitedPosts } from '../../modules/common/types';

import PostItem from './PostItem';

type PostListProps = {
  posts: Array<Post>;
  visitedPosts: VisitedPosts;
  dismissedPosts: DismissedPosts;
  onPostDismiss: (post: Post) => void;
};

const PostList: React.FC<PostListProps> = ({
  posts,
  visitedPosts,
  dismissedPosts,
  onPostDismiss,
}: PostListProps) => {
  const [data] = useState<Array<Post>>(posts);

  return (
    <>
      {data.map(post => (
        <PostItem
          key={post.name}
          post={post}
          visited={isPostVisited(visitedPosts, post)}
          dismissed={isPostDismissed(dismissedPosts, post)}
          onDismiss={onPostDismiss}
        />
      ))}
    </>
  );
};

export default PostList;
