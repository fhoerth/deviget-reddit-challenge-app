import React, { useEffect } from 'react';
import { Dispatch } from 'redux';
import { useDispatch, useSelector } from 'react-redux';

import { Post } from '../../modules/reddit/types';

import Error from '../../modules/common/components/Error';
import Loading from '../../modules/common/components/Loading';
import * as actions from '../../modules/store/topPosts/actions';
import { RootState } from '../../modules/store/types';
import { TopPostsAction } from '../../modules/store/topPosts/types';

import DismissAll from './DismissAll';
import PostList from './PostList';
import FetchNextTopPosts from './FetchNextTopPosts';

import styles from './styles/TopPosts.scss';

const TopPosts: React.FC = () => {
  const dispatch = useDispatch<Dispatch<TopPostsAction>>();

  const fetching = useSelector<RootState, boolean>(state => state.topPosts.fetchingTopPosts);
  const fulfilled = useSelector<RootState, boolean>(state => state.topPosts.fetchTopPostsFulfilled);
  const rejected = useSelector<RootState, boolean>(state => state.topPosts.fetchTopPostsRejected);
  const posts = useSelector<RootState, Array<Post> | null>(state => state.topPosts.data);

  const visited = useSelector<RootState, Array<string>>(state => state.topPosts.visited);
  const dismissed = useSelector<RootState, Array<string>>(state => state.topPosts.dismissed);
  const allDismissed = useSelector<RootState, boolean>(state => state.topPosts.allDismissed);

  const renderPostsList = !!(fulfilled && posts);

  useEffect(() => {
    if (!allDismissed) {
      dispatch(actions.fetchTopPosts());
    }
  }, [allDismissed]);

  const handlePostDismiss = (post: Post): void => {
    dispatch(actions.dismissTopPost({ post }));
  };

  const handleDismissAll = (): void => {
    if (!allDismissed) {
      dispatch(actions.dismissAllTopPosts());
    }
  };

  const handleFetchNextTopPosts = (): void => {
    dispatch(actions.undismissAllTopPosts());
    dispatch(actions.fetchTopPosts());
  };

  return (
    <div className={styles.posts}>
      <div className={styles.scrolleable}>
        {rejected && <Error message="An error occurred while fetching top posts :(." />}
        {fetching && <Loading theme="light" />}
        {allDismissed && (
          <FetchNextTopPosts
            animate={renderPostsList}
            onFetchNextTopPosts={handleFetchNextTopPosts}
          />
        )}
        {renderPostsList && (
          <PostList
            posts={posts || []}
            visitedPosts={visited}
            dismissedPosts={dismissed}
            onPostDismiss={handlePostDismiss}
          />
        )}
      </div>
      <DismissAll onDismissAll={handleDismissAll} disabled={!renderPostsList || allDismissed} />
    </div>
  );
};

export default TopPosts;
