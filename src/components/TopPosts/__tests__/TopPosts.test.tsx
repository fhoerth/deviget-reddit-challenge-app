import React from 'react';
import configureStore from 'redux-mock-store';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import { MemoryRouter } from 'react-router';

import Error from '../../../modules/common/components/Error';
import Loading from '../../../modules/common/components/Loading';
import { DismissedPosts, VisitedPosts } from '../../../modules/common/types';

import { RootState } from '../../../modules/store/types';
import { defaultState } from '../../../modules/store/topPosts/reducer';
import * as actions from '../../../modules/store/topPosts/actions';

import TopPosts from '..';
import FetchNextTopPosts from '../FetchNextTopPosts';

import posts from './mocks/posts.json';
import PostList from '../PostList';

const mockStore = configureStore<Pick<RootState, 'topPosts'>>();

describe('TopPosts component', () => {
  describe("when posts aren't dismissed", () => {
    it('dispatches fetchTopPosts action', () => {
      const store = mockStore({ topPosts: defaultState });

      mount(
        <Provider store={store}>
          <TopPosts />
        </Provider>,
      );

      const storeActions = store.getActions();

      expect(storeActions).toEqual([
        actions.fetchTopPosts(),
      ]);
    });
  });

  describe('when posts are dismissed', () => {
    it('renders FetchNextTopPosts component', () => {
      const store = mockStore({
        topPosts: {
          ...defaultState,
          allDismissed: true,
        },
      });

      const wrapper = mount(
        <Provider store={store}>
          <TopPosts />
        </Provider>,
      );

      const storeActions = store.getActions();

      expect(storeActions).toHaveLength(0);
      expect(wrapper.find(FetchNextTopPosts)).toHaveLength(1);
    });
  });

  describe('while fetching', () => {
    it('renders a loading state', () => {
      const store = mockStore({
        topPosts: {
          ...defaultState,
          fetchingTopPosts: true,
        },
      });

      const wrapper = mount(
        <Provider store={store}>
          <TopPosts />
        </Provider>,
      );

      expect(wrapper.find(Loading)).toHaveLength(1);
    });
  });

  describe('when fetch is rejected', () => {
    it('renders an error message', () => {
      const store = mockStore({
        topPosts: {
          ...defaultState,
          fetchTopPostsRejected: true,
        },
      });

      const wrapper = mount(
        <Provider store={store}>
          <TopPosts />
        </Provider>,
      );

      expect(wrapper.find(Error)).toHaveLength(1);
    });
  });

  describe('when fetch is fulfilled', () => {
    it('renders PostList component passing expected props', () => {
      /**
       * Just check TopPosts is passing object's reference.
       */
      const dismissed: DismissedPosts = [];
      const visited: VisitedPosts = [];

      const store = mockStore({
        topPosts: {
          ...defaultState,
          visited,
          dismissed,
          fetchTopPostsFulfilled: true,
          data: posts,
        },
      });

      const wrapper = mount(
        <MemoryRouter>
          <Provider store={store}>
            <TopPosts />
          </Provider>
        </MemoryRouter>,
      );

      const postList = wrapper.find(PostList);

      expect(postList).toHaveLength(1);
      expect(postList.props()).toMatchObject({
        posts,
        visitedPosts: visited,
        dismissedPosts: dismissed,
      });
    });
  });
});
