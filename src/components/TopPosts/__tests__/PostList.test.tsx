import React from 'react';
import fakeTimers from '@sinonjs/fake-timers';
import { MemoryRouter } from 'react-router';
import { shallow } from 'enzyme';

import PostList from '../PostList';
import PostItem from '../PostItem';

import postsMock from './mocks/posts.json';

describe('PostList component', () => {
  const posts = postsMock.slice(0, 5);
  const visitedPosts = [posts[0].id];
  const dismissedPosts = [posts[1].id];

  it('renders a list of PostItem components', () => {
    const clock = fakeTimers.install({
      now: new Date(2020, 6, 18),
    });

    const handlePostDismiss = jest.fn();

    const wrapper = shallow(
      <MemoryRouter>
        <PostList
          posts={posts}
          visitedPosts={visitedPosts}
          dismissedPosts={dismissedPosts}
          onPostDismiss={handlePostDismiss}
        />
      </MemoryRouter>,
    );

    expect(wrapper.html()).toMatchSnapshot();

    clock.uninstall();
  });

  it('passes expected props to PostItem components', () => {
    const handlePostDismiss = jest.fn();

    const wrapper = shallow(
      <MemoryRouter>
        <PostList
          posts={posts}
          visitedPosts={visitedPosts}
          dismissedPosts={dismissedPosts}
          onPostDismiss={handlePostDismiss}
        />
      </MemoryRouter>,
    );

    const postComponentListWrapper = wrapper.find(PostList).dive();
    const props = {
      visited: false,
      dismissed: false,
      onDismiss: handlePostDismiss,
    };

    const [
      visitedPost,
      dismissedPost,
      ...remainingPosts
    ] = [
      postComponentListWrapper.find(PostItem).get(0),
      postComponentListWrapper.find(PostItem).get(1),
      postComponentListWrapper.find(PostItem).get(2),
      postComponentListWrapper.find(PostItem).get(3),
      postComponentListWrapper.find(PostItem).get(4),
    ];

    expect(visitedPost.props).toMatchObject({
      ...props,
      post: posts[0],
      visited: true,
    });

    expect(dismissedPost.props).toMatchObject({
      ...props,
      post: posts[1],
      dismissed: true,
    });

    expect(remainingPosts.map(post => post.props)).toEqual([
      {
        ...props,
        post: posts[2],
      },
      {
        ...props,
        post: posts[3],
      },
      {
        ...props,
        post: posts[4],
      },
    ]);
  });

  describe('when `posts` prop changes', () => {
    it('does not unmount children', () => {
      const handlePostDismiss = jest.fn();
      const newPosts = postsMock.slice(0, 2);

      const wrapper = shallow(
        <PostList
          posts={posts}
          visitedPosts={visitedPosts}
          dismissedPosts={dismissedPosts}
          onPostDismiss={handlePostDismiss}
        />,
      );

      wrapper.setProps({
        visitedPosts,
        dismissedPosts,
        posts: newPosts,
        onPostDismiss: handlePostDismiss,
      });

      expect(wrapper.find(PostItem).length).toBe(5);
    });
  });
});
