import React from 'react';
import waait from 'waait';
import fakeTimers from '@sinonjs/fake-timers';
import { act } from 'react-dom/test-utils';
import { MemoryRouter } from 'react-router';
import { mount } from 'enzyme';

import createPostMock from '../../../modules/common/testUtils';
import post from './mocks/post.json';

import PostItem from '../PostItem';

describe('PostItem component', () => {
  it('renders post data', () => {
    const clock = fakeTimers.install({
      now: new Date(2020, 6, 18),
    });

    const handleDismiss = jest.fn();

    const firstWrapper = mount(
      <MemoryRouter>
        <PostItem
          visited
          post={post}
          dismissed={false}
          onDismiss={handleDismiss}
        />
      </MemoryRouter>,
    );

    const secondWrapper = mount(
      <MemoryRouter>
        <PostItem
          post={post}
          visited={false}
          dismissed={false}
          onDismiss={handleDismiss}
        />
      </MemoryRouter>,
    );

    expect(firstWrapper.html()).toMatchSnapshot();
    expect(secondWrapper.html()).toMatchSnapshot();

    clock.uninstall();
  });

  it('returns null after animation is completed', async () => {
    const postMock = createPostMock();
    const handleDismiss = jest.fn();

    const wrapper = mount(
      <MemoryRouter>
        <PostItem
          dismissed
          post={postMock}
          visited={false}
          onDismiss={handleDismiss}
        />
      </MemoryRouter>,
    );

    await act(async () => {
      await waait(500);

      wrapper.update();

      expect(wrapper.find(PostItem).isEmptyRender()).toBe(true);
    });
  });

  it('calls onDismiss prop when "dismiss" button is clicked', () => {
    const postMock = createPostMock();
    const handleDismiss = jest.fn();

    const wrapper = mount(
      <MemoryRouter>
        <PostItem
          dismissed
          post={postMock}
          visited={false}
          onDismiss={handleDismiss}
        />
      </MemoryRouter>,
    );

    // Simulate click on dismiss post.
    wrapper.find(PostItem).find('div.dismissPost').simulate('click');

    expect(handleDismiss.mock.calls).toHaveLength(1);

    const [firstCallArgs] = handleDismiss.mock.calls;

    expect(firstCallArgs).toHaveLength(1);

    const [postArg] = firstCallArgs;

    expect(postArg).toBe(postMock);
  });
});
