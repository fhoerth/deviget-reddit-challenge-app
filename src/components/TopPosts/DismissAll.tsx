import React from 'react';
import classnames from 'classnames';

import styles from './styles/DismissAll.scss';

type DismissAllProps = {
  disabled: boolean;
  onDismissAll: () => void;
};

const DismissAll: React.SFC<DismissAllProps> = ({ disabled, onDismissAll }: DismissAllProps) => {
  const handleClick = (): void => {
    if (!disabled) {
      onDismissAll();
    }
  };

  return (
    <div
      className={classnames(styles.dismissAll, disabled ? styles.dismissAllDisabled : null)}
      onClick={handleClick}
    >
      <span>Dismiss All</span>
    </div>
  );
};

export default DismissAll;
