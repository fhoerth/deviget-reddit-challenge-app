import React from 'react';
import configureStore from 'redux-mock-store';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import Error from '../../../modules/common/components/Error';
import Loading from '../../../modules/common/components/Loading';

import { RootState } from '../../../modules/store/types';
import { defaultState as topPostsDefaultState } from '../../../modules/store/topPosts/reducer';
import { defaultState as postDefaultState } from '../../../modules/store/post/reducer';
import * as actions from '../../../modules/store/post/actions';

import Post from '..';

import post from './mocks/post.json';

const mockStore = configureStore<Pick<RootState, 'post' | 'topPosts'>>();

describe('Post component', () => {
  it('dispatches fetchPost action', () => {
    const store = mockStore({
      topPosts: topPostsDefaultState,
      post: postDefaultState,
    });

    mount(
      <Provider store={store}>
        <Post id={post.id} />
      </Provider>,
    );

    const storeActions = store.getActions();

    expect(storeActions).toEqual([
      actions.fetchPost({ id: post.id }),
    ]);
  });

  describe('while fetching', () => {
    it('renders a loading state', () => {
      const store = mockStore({
        topPosts: topPostsDefaultState,
        post: {
          ...postDefaultState,
          fetchingPost: true,
        },
      });

      const wrapper = mount(
        <Provider store={store}>
          <Post id={post.id} />
        </Provider>,
      );

      expect(wrapper.find(Loading)).toHaveLength(1);
    });
  });

  describe('when fetch is rejected', () => {
    it('renders an error message', () => {
      const store = mockStore({
        topPosts: topPostsDefaultState,
        post: {
          ...postDefaultState,
          fetchPostFulfilled: false,
          fetchPostRejected: true,
        },
      });

      const wrapper = mount(
        <Provider store={store}>
          <Post id={post.id} />
        </Provider>,
      );

      const error = wrapper.find(Error);

      expect(error).toHaveLength(1);
      expect(error.props().message).toBe(`An error occurred while fetching post "${post.id}" :(.`);
    });
  });

  describe('when fetch is fulfilled', () => {
    describe('and post is dismissed', () => {
      it('renders an error message', () => {
        const store = mockStore({
          topPosts: {
            ...topPostsDefaultState,
            dismissed: [post.id],
          },
          post: {
            ...postDefaultState,
            data: post,
            fetchPostFulfilled: true,
          },
        });

        const wrapper = mount(
          <Provider store={store}>
            <Post id={post.id} />
          </Provider>,
        );

        const error = wrapper.find(Error);

        expect(error).toHaveLength(1);
        expect(error.props().message).toContain('dismissed');
      });
    });

    describe("and post isn't dismissed", () => {
      it('renders post data', () => {
        const store = mockStore({
          topPosts: topPostsDefaultState,
          post: {
            ...postDefaultState,
            data: post,
            fetchPostFulfilled: true,
          },
        });

        const wrapper = mount(
          <Provider store={store}>
            <Post id={post.id} />
          </Provider>,
        );

        expect(wrapper.html()).toMatchSnapshot();
      });
    });
  });
});
