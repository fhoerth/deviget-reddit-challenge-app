import React, { useEffect } from 'react';
import { Dispatch } from 'redux';
import { useDispatch, useSelector } from 'react-redux';

import Error from '../../modules/common/components/Error';
import Loading from '../../modules/common/components/Loading';
import { isPostDismissed, unescapeHtml } from '../../modules/common/utils';
import { Post as PostType } from '../../modules/reddit/types';

import * as postActions from '../../modules/store/post/actions';
import { PostAction } from '../../modules/store/post/types';

import * as topPostsActions from '../../modules/store/topPosts/actions';
import { TopPostsAction } from '../../modules/store/topPosts/types';

import { RootState } from '../../modules/store/types';

import styles from './styles/Post.scss';

type PostProps = {
  id: string;
};

const Post: React.FC<PostProps> = ({ id }: PostProps) => {
  const dispatch = useDispatch<Dispatch<PostAction | TopPostsAction>>();

  const fetching = useSelector<RootState, boolean>(state => state.post.fetchingPost);
  const fulfilled = useSelector<RootState, boolean>(state => state.post.fetchPostFulfilled);
  const rejected = useSelector<RootState, boolean>(state => state.post.fetchPostRejected);
  const post = useSelector<RootState, PostType | null>(state => state.post.data);
  const postDismissed = useSelector<RootState, boolean>(state => (
    !!post && (isPostDismissed(state.topPosts.dismissed, post))
  ));

  useEffect(() => {
    dispatch(postActions.fetchPost({ id }));
  }, [id]);

  useEffect(() => {
    if (post) {
      dispatch(topPostsActions.visitTopPost({ post }));
    }
  }, [post]);

  if (rejected) {
    const message = `An error occurred while fetching post "${id}" :(.`;
    return <Error message={message} />;
  }

  if (fetching) {
    return <Loading theme="dark" />;
  }

  if (fulfilled && postDismissed) {
    return <Error message="This post is dismissed." />;
  }

  if (fulfilled && post) {
    const {
      title,
      thumbnail,
      author,
      thumbnail_height: thumbnailHeight,
      thumbnail_width: thumbnailWidth,
      selftext_html: selftextHtml,
    } = post;
    const unescapedHtml = selftextHtml ? unescapeHtml(selftextHtml) : null;
    const content = unescapedHtml ? (
      <>
        {/* eslint-disable-next-line react/no-danger */}
        <div className={styles.postContent} dangerouslySetInnerHTML={{ __html: unescapedHtml }} />
      </>
    ) : null;

    return (
      <article className={styles.post}>
        <h5 className={styles.author}>{author}</h5>
        <h1 className={styles.title}>{title}</h1>
        {thumbnail && thumbnailHeight && thumbnailWidth && (
          <img
            alt={title}
            className={styles.thumbnail}
            src={post.thumbnail}
            width={thumbnailWidth}
            height={thumbnailHeight}
          />
        )}
        {content}
      </article>
    );
  }

  return null;
};

export default Post;
