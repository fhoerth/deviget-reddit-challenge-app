import React from 'react';
import { Provider } from 'react-redux';
import { hot } from 'react-hot-loader/root';

import createStore from '../../modules/store';
import Router from '../Router';

// Import global styles.
import './styles/App.scss';

const App: React.SFC = () => {
  const store = createStore();

  return (
    <Provider store={store}>
      <Router />
    </Provider>
  );
};

export default hot(App);
