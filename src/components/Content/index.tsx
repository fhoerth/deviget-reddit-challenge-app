import React from 'react';

import styles from './styles/Content.scss';

type ContentProps = {
  children: React.ReactNode;
};

const Content: React.SFC<ContentProps> = ({ children }: ContentProps) => (
  <main className={styles.content}>
    {children}
  </main>
);

export default Content;
