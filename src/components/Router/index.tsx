import React from 'react';
import {
  BrowserRouter,
  Switch,
  Route,
  RouteComponentProps,
} from 'react-router-dom';

import { PostRouteParams } from './types';

import Layout from '../Layout';
import Main from '../Main';
import Post from '../Post';

import routes from './routes';

const Router: React.SFC = () => (
  <BrowserRouter>
    <Switch>
      <Layout>
        <Route exact path={routes.home.path} component={Main} />

        <Route
          exact
          path={routes.post.path}
          render={(context: RouteComponentProps<PostRouteParams>): React.ReactNode => (
            <Post id={context.match.params.id} />
          )}
        />
      </Layout>
    </Switch>
  </BrowserRouter>
);

export default Router;
