const routes = {
  home: {
    name: 'home',
    path: '/',
  },
  post: {
    name: 'post',
    path: '/post/:id/:slug',
  },
};

export default routes;
