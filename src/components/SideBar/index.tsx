import React from 'react';
import classnames from 'classnames';

import TopPosts from '../TopPosts';

import styles from './styles/SideBar.scss';

type SideBarProps = {
  sm?: boolean;
  md?: boolean;
  lg?: boolean;
  show: boolean;
  onSideBarClose?: () => void;
};

const SideBar: React.SFC<SideBarProps> = ({
  sm,
  md,
  lg,
  show,
  onSideBarClose,
}: SideBarProps) => {
  const handleSideBarClose = (): void => {
    if (onSideBarClose) {
      onSideBarClose();
    }
  };

  const className = classnames({
    [styles.sideBarSm]: sm,
    [styles.sideBarMd]: md,
    [styles.sideBarLg]: lg,
    [styles.sideBarShow]: show,
  });

  return (
    <div className={className}>
      <div className={styles.iconWrapper}>
        <span className={styles.icon} onClick={handleSideBarClose} />
      </div>
      <TopPosts />
    </div>
  );
};


export default SideBar;
