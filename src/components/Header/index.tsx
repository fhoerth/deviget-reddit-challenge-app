import React from 'react';

import styles from './styles/Header.scss';

type HeaderProps = {
  onSideBarOpen: () => void;
};

const Header: React.SFC<HeaderProps> = ({ onSideBarOpen }: HeaderProps) => (
  <header className={styles.header}>
    <span className={styles.icon} onClick={onSideBarOpen} />
    <h1 className={styles.title}>Reddit application</h1>
  </header>
);

export default Header;
