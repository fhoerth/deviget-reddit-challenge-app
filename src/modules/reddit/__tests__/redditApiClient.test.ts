import nock from 'nock';
import {
  endpoints,
  fetchTopPosts,
  fetchPost,
} from '../apiClient';
import { withQueryParams } from '../utils';
import config from '../config';

import topPostsMock from './mocks/top.json';
import postMock from './mocks/t3_gxslm9.json';

const id = 'ft81jce';

describe('reddit apiClient', () => {
  describe('fetchPost', () => {
    it('resolves a post', async () => {
      nock(config.baseURL)
        .get(endpoints.post(id).replace(config.baseURL, ''))
        .reply(200, postMock);

      const post = await fetchPost(id);
      const expectedId = 'gxslm9';

      // Assert on post id to make sure it retrieves desired data.
      expect(post.id).toBe(expectedId);
    });

    it('handles response errors properly', async (done) => {
      nock(config.baseURL)
        .get(endpoints.post(id).replace(config.baseURL, ''))
        .reply(404);

      try {
        await fetchPost(id);
      } catch (error) {
        expect(error.message).toEqual('Not Found');
        done();
      }
    });
  });

  describe('fetchTopPosts', () => {
    it('resolves top posts', async () => {
      nock(config.baseURL)
        .get(withQueryParams(endpoints.topPosts.replace(config.baseURL, ''), { limit: config.topPosts.limit }))
        .reply(200, topPostsMock);

      const posts = await fetchTopPosts();
      const expectedIds = topPostsMock.data.children.map(t3 => t3.data.id);

      // Assert on post ids to make sure it retrieves desired data.
      expect(posts.map(post => post.id)).toEqual(expectedIds);
    });

    it('accepts query params', async () => {
      const queryParams = { limit: 100, after: 'abc123' };

      nock(config.baseURL)
        .get(withQueryParams(endpoints.topPosts.replace(config.baseURL, ''), queryParams))
        .reply(200, topPostsMock);

      const posts = await fetchTopPosts(queryParams);
      const expectedIds = topPostsMock.data.children.map(t3 => t3.data.id);

      // Assert on post ids to make sure it retrieves desired data.
      expect(posts.map(post => post.id)).toEqual(expectedIds);
    });

    it('handles response errors properly', async (done) => {
      nock(config.baseURL)
        .get(withQueryParams(endpoints.topPosts.replace(config.baseURL, ''), { limit: config.topPosts.limit }))
        .reply(404);

      try {
        await fetchTopPosts();
      } catch (error) {
        expect(error.message).toEqual('Not Found');
        done();
      }
    });
  });
});
