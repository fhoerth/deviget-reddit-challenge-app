import isomorficFetch from 'isomorphic-fetch';
import retry from 'fetch-retry';

import {
  Post,
  PostResponse,
  TopPostsResponse,
  TopPostsQueryParams,
} from './types';
import { withQueryParams, handleResponseError } from './utils';
import config from './config';

const fetch = retry(isomorficFetch);
const fetchTopPostsDefaultQueryParams = { limit: config.topPosts.limit };
const retryStrategy = { retries: config.retry.times, retryDelay: config.retry.delay };

export const endpoints = {
  topPosts: `${config.baseURL}/top.json`,
  post: (id: string): string => `${config.baseURL}/${id}.json`,
};

export const fetchTopPosts = (queryParams?: TopPostsQueryParams): Promise<Array<Post>> => fetch(
  withQueryParams(endpoints.topPosts, queryParams || fetchTopPostsDefaultQueryParams),
  retryStrategy,
).then(
  handleResponseError,
).then(
  response => response.json(),
).then(
  (response: TopPostsResponse) => response.data.children.map(t3 => t3.data),
);

export const fetchPost = (id: string): Promise<Post> => fetch(
  endpoints.post(id),
  retryStrategy,
).then(
  handleResponseError,
).then(
  response => response.json(),
).then(
  (response: PostResponse) => {
    const [postListing] = response;
    // First object in the array is the post.
    const [t3] = postListing.data.children;

    return t3.data;
  },
);
