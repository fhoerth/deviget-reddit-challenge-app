const config = {
  baseURL: 'https://www.reddit.com',
  topPosts: {
    limit: 50,
  },
  retry: {
    times: 3,
    delay: 300,
  },
};

export default config;
