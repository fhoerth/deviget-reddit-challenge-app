/* eslint camelcase: 0 */
export type PostImage = {
  url: string;
  width: number;
  height: number;
};
export type Post = {
  id: string;
  name: string;
  author: string;
  author_fullname: string;
  title: string;
  subreddit_name_prefixed: string;
  thumbnail: 'self' | string;
  thumbnail_width: number | null;
  thumbnail_height: number | null;
  selftext_html: string | null;
  num_comments: number;
  created_utc: number;
  permalink: string;
  url: string;
  preview?: {
    images: Array<{
      resolutions: Array<PostImage>;
    }>;
  };
};
export type T3 = {
  kind: 't3';
  data: Post;
};

// Just an empty object type since showing comments isn't requested feature.
type PostComment = Record<string, unknown>;
type T1 = {
  kind: 't1';
  data: PostComment;
};

/**
 * Top Posts
 */
export type PostsListing = {
  kind: 'Listing';
  data: {
    dist: number;
    modhash: string;
    children: Array<T3>;
    after: string | null;
    before: string | null;
  };
};
export type TopPostsResponse = PostsListing;
export type TopPostsQueryParams = Partial<{
  after: string;
  limit: number;
}>;
/**
 * Post by id
 */
export type PostListing = {
  kind: 'Listing';
  data: {
    dist: 1;
    modhash: string;
    children: [T3],
    after: null;
    before: null;
  };
};
export type CommentsListing = {
  kind: 'Listing';
  data: {
    dist: null;
    modhash: string;
    children: Array<T1>;
  }
};
export type PostResponse = [PostListing, CommentsListing];
