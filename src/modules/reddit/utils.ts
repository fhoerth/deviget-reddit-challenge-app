import queryString from 'query-string';

import ResponseError from './errors/ResponseError';
import { TopPostsQueryParams } from './types';

export function withQueryParams(endpoint: string, queryParams: TopPostsQueryParams): string {
  return endpoint.concat('?').concat(queryString.stringify(queryParams));
}

export function handleResponseError(response: Response): Response {
  if (!response.ok) {
    throw new ResponseError(response);
  }

  return response;
}

export default handleResponseError;
