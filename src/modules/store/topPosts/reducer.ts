import {
  addVisitedPost,
  addDismissedPost,
  addDismissedPosts,
  excludePost,
} from '../../common/utils';

import constants from './constants';
import { TopPostsAction, TopPostsState } from './types';

export const defaultState: TopPostsState = {
  fetchTopPostsFulfilled: false,
  fetchTopPostsRejected: false,
  fetchingTopPosts: false,
  visited: [],
  dismissed: [],
  allDismissed: false,
  data: null,
};

function topPostsReducer(
  state: TopPostsState = defaultState,
  action: TopPostsAction,
): TopPostsState {
  switch (action.type) {
    case constants.HYDRATE_TOP_POSTS_DATA: {
      const { payload } = action;
      const { data } = payload;
      const { visited, dismissed, allDismissed } = data;

      return {
        ...state,
        visited,
        dismissed,
        allDismissed,
      };
    }

    case constants.FETCH_TOP_POSTS: {
      return {
        ...state,
        fetchingTopPosts: true,
        fetchTopPostsFulfilled: false,
        fetchTopPostsRejected: false,
      };
    }

    case constants.FETCH_TOP_POSTS_FULFILLED: {
      const { payload } = action;
      const { posts } = payload;

      return {
        ...state,
        data: posts,
        fetchTopPostsFulfilled: true,
        fetchTopPostsRejected: false,
        fetchingTopPosts: false,
      };
    }

    case constants.FETCH_TOP_POSTS_REJECTED: {
      return {
        ...state,
        fetchTopPostsFulfilled: false,
        fetchTopPostsRejected: true,
        fetchingTopPosts: false,
      };
    }

    case constants.VISIT_TOP_POST: {
      const { payload } = action;
      const { post } = payload;

      return {
        ...state,
        visited: addVisitedPost(state.visited, post),
      };
    }

    case constants.DISMISS_TOP_POST: {
      const { payload } = action;
      const { post } = payload;
      const { data } = state;

      if (data) {
        const filteredData = excludePost(data, post);

        return {
          ...state,
          data: filteredData,
          allDismissed: !filteredData.length,
          dismissed: addDismissedPost(state.dismissed, post),
        };
      }

      return state;
    }

    case constants.DISMISS_ALL_TOP_POSTS: {
      const { data } = state;

      if (data) {
        const dismissed = addDismissedPosts(state.dismissed, data);

        return {
          ...state,
          dismissed,
          data: [],
          allDismissed: true,
        };
      }

      return state;
    }

    case constants.UNDISMISS_ALL_TOP_POSTS: {
      return {
        ...state,
        allDismissed: false,
      };
    }

    default:
      return state;
  }
}

export default topPostsReducer;
