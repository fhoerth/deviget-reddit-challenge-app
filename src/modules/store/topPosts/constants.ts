enum Constants {
  FETCH_TOP_POSTS = '@@posts/FETCH_TOP_POSTS',
  FETCH_TOP_POSTS_FULFILLED = '@@posts/FETCH_TOP_POSTS_FULFILLED',
  FETCH_TOP_POSTS_REJECTED = '@@posts/FETCH_TOP_POSTS_REJECTED',
  VISIT_TOP_POST = '@@posts/VISIT_TOP_POST',
  DISMISS_TOP_POST = '@@posts/DISMISS_TOP_POST',
  DISMISS_ALL_TOP_POSTS = '@@posts/DISMISS_ALL_TOP_POSTS',
  UNDISMISS_ALL_TOP_POSTS = '@@posts/UNDISMISS_ALL_TOP_POSTS',
  PERSIST_TOP_POSTS_DATA = '@@posts/PERSIST_TOP_POSTS_DATA',
  HYDRATE_TOP_POSTS_DATA = '@@posts/HYDRATE_TOP_POSTS_DATA',
}

export default Constants;
