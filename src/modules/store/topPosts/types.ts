import { Post } from '../../reddit/types';
import { DismissedPosts, VisitedPosts } from '../../common/types';

import constants from './constants';

export type TopPostsState = {
  fetchingTopPosts: boolean;
  fetchTopPostsRejected: boolean;
  fetchTopPostsFulfilled: boolean;
  visited: VisitedPosts;
  dismissed: DismissedPosts;
  allDismissed: boolean;
  data: Array<Post> | null;
};

export type TopPostsPersistedData = Pick<TopPostsState, 'visited' | 'dismissed' | 'allDismissed'>;

export type FetchTopPostsAction = {
  type: constants.FETCH_TOP_POSTS;
};

export type FetchTopPostsFulfilledAction = {
  type: constants.FETCH_TOP_POSTS_FULFILLED;
  payload: {
    posts: Array<Post>;
  };
};

export type FetchTopPostsRejectedAction = {
  type: constants.FETCH_TOP_POSTS_REJECTED;
};

export type VisitTopPostAction = {
  type: constants.VISIT_TOP_POST;
  payload: {
    post: Post;
  };
};

export type DismissTopPostAction = {
  type: constants.DISMISS_TOP_POST;
  payload: {
    post: Post;
  };
};

export type DismissAllTopPostsAction = {
  type: constants.DISMISS_ALL_TOP_POSTS;
};

export type UndismissAllTopPostsAction = {
  type: constants.UNDISMISS_ALL_TOP_POSTS;
};

export type HydrateTopPostsDataAction = {
  type: constants.HYDRATE_TOP_POSTS_DATA;
  payload: {
    data: TopPostsPersistedData;
  }
};

export type PersistTopPostsDataAction = {
  type: constants.PERSIST_TOP_POSTS_DATA;
};

export type TopPostsAction = FetchTopPostsAction
  | FetchTopPostsRejectedAction
  | FetchTopPostsFulfilledAction
  | VisitTopPostAction
  | DismissTopPostAction
  | DismissAllTopPostsAction
  | UndismissAllTopPostsAction
  | HydrateTopPostsDataAction
  | PersistTopPostsDataAction;
