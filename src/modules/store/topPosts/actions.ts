import {
  FetchTopPostsAction,
  FetchTopPostsRejectedAction,
  FetchTopPostsFulfilledAction,
  VisitTopPostAction,
  DismissTopPostAction,
  DismissAllTopPostsAction,
  UndismissAllTopPostsAction,
  HydrateTopPostsDataAction,
  PersistTopPostsDataAction,
} from './types';

import constants from './constants';

export const fetchTopPosts = (): FetchTopPostsAction => ({
  type: constants.FETCH_TOP_POSTS,
});

export const fetchTopPostsFulfilled = (
  payload: FetchTopPostsFulfilledAction['payload'],
): FetchTopPostsFulfilledAction => ({
  payload,
  type: constants.FETCH_TOP_POSTS_FULFILLED,
});

export const fetchTopPostsRejected = (): FetchTopPostsRejectedAction => ({
  type: constants.FETCH_TOP_POSTS_REJECTED,
});

export const visitTopPost = (payload: VisitTopPostAction['payload']): VisitTopPostAction => ({
  payload,
  type: constants.VISIT_TOP_POST,
});

export const dismissTopPost = (payload: DismissTopPostAction['payload']): DismissTopPostAction => ({
  payload,
  type: constants.DISMISS_TOP_POST,
});

export const dismissAllTopPosts = (): DismissAllTopPostsAction => ({
  type: constants.DISMISS_ALL_TOP_POSTS,
});

export const undismissAllTopPosts = (): UndismissAllTopPostsAction => ({
  type: constants.UNDISMISS_ALL_TOP_POSTS,
});

export const hydrateTopPostsData = (
  payload: HydrateTopPostsDataAction['payload'],
): HydrateTopPostsDataAction => ({
  payload,
  type: constants.HYDRATE_TOP_POSTS_DATA,
});

export const persistTopPostsData = (): PersistTopPostsDataAction => ({
  type: constants.PERSIST_TOP_POSTS_DATA,
});
