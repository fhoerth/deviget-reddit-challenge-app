import * as ls from 'local-storage';
import { TopPostsPersistedData } from './types';

const key = '@@deviget-reddit-app/persisted-store-data';

export const get = (): TopPostsPersistedData | null => {
  const data = ls.get<TopPostsPersistedData | null>(key);

  return data;
};

export const set = (data: TopPostsPersistedData): void => {
  ls.set<TopPostsPersistedData>(key, data);
};

export const clear = (): void => {
  ls.clear();
};
