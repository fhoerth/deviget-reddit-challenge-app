import { SagaIterator } from 'redux-saga';
import {
  call,
  put,
  takeEvery,
  select,
} from 'redux-saga/effects';

import config from '../../reddit/config';
import * as apiClient from '../../reddit/apiClient';
import { Post } from '../../reddit/types';
import { excludeDismissedPosts } from '../../common/utils';

import { RootState } from '../types';

import * as storage from './storage';
import * as actions from './actions';
import constants from './constants';

const getVisitedPosts = (state: RootState): Array<string> => state.topPosts.visited;
const getDimissedPosts = (state: RootState): Array<string> => state.topPosts.dismissed;
const getAllDismissed = (state: RootState): boolean => state.topPosts.allDismissed;

export function* fetchFirstNonDismissedTopPostsSaga(
  after: string | null = null,
  accumulatedPosts: Array<Post> = [],
): SagaIterator<Array<Post>> {
  const dismissedPosts = getDimissedPosts(yield select());

  const queryParams = {
    ...(after ? { after } : null),
    limit: config.topPosts.limit,
  };

  const nextPosts: Array<Post> = yield call(
    apiClient.fetchTopPosts,
    queryParams,
  );

  const undismissedNextPosts = excludeDismissedPosts(nextPosts, dismissedPosts);

  const posts = [...accumulatedPosts, ...undismissedNextPosts];

  if (posts.length < config.topPosts.limit) {
    const lastUndismissedPost = undismissedNextPosts[undismissedNextPosts.length - 1];

    if (lastUndismissedPost) {
      return yield call(fetchFirstNonDismissedTopPostsSaga, lastUndismissedPost.name, posts);
    }

    /**
     * Fetch next page, this means all posts from the current page have been dismissed.
     */
    const lastFetchedPost = nextPosts[nextPosts.length - 1];

    if (lastFetchedPost) {
      return yield call(fetchFirstNonDismissedTopPostsSaga, lastFetchedPost.name);
    }
  }

  return posts.slice(0, config.topPosts.limit);
}

export function* fetchTopPostsSaga(): SagaIterator<void> {
  try {
    const posts: Array<Post> = yield call(fetchFirstNonDismissedTopPostsSaga);

    yield put(actions.fetchTopPostsFulfilled({ posts }));
  } catch (e) {
    yield put(actions.fetchTopPostsRejected());
  }
}

export function* watchFetchTopPostsSaga(): SagaIterator<void> {
  yield takeEvery(constants.FETCH_TOP_POSTS, fetchTopPostsSaga);
}

export function* persistTopPostsSaga(): SagaIterator<void> {
  const visited = getVisitedPosts(yield select());
  const dismissed = getDimissedPosts(yield select());
  const allDismissed = getAllDismissed(yield select());

  storage.set({ visited, dismissed, allDismissed });
}

export function* watchPersistTopPostsSaga(): SagaIterator<void> {
  yield takeEvery(constants.DISMISS_TOP_POST, persistTopPostsSaga);
  yield takeEvery(constants.DISMISS_ALL_TOP_POSTS, persistTopPostsSaga);
  yield takeEvery(constants.VISIT_TOP_POST, persistTopPostsSaga);
  yield takeEvery(constants.FETCH_TOP_POSTS, persistTopPostsSaga);
}

export function* hydrateTopPostsSaga(): SagaIterator<void> {
  const data = storage.get();

  if (data) {
    yield put(actions.hydrateTopPostsData({ data }));
  }
}
