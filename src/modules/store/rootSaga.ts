import { all } from 'redux-saga/effects';

import { watchFetchPostSaga } from './post/sagas';
import { hydrateTopPostsSaga, watchPersistTopPostsSaga, watchFetchTopPostsSaga } from './topPosts/sagas';

function createRootSaga(): () => Generator {
  function* rootSaga(): Generator {
    yield all([
      hydrateTopPostsSaga(),
      watchPersistTopPostsSaga(),
      watchFetchPostSaga(),
      watchFetchTopPostsSaga(),
    ]);
  }

  return rootSaga;
}

export default createRootSaga;
