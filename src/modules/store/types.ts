import { PostState, PostAction } from './post/types';
import { SideBarState, SideBarAction } from './sideBar/types';
import { TopPostsState, TopPostsAction } from './topPosts/types';

export type RootState = {
  post: PostState;
  sideBar: SideBarState;
  topPosts: TopPostsState;
};

export type RootAction = SideBarAction | PostAction | TopPostsAction;
