import constants from './constants';
import { SideBarState, SideBarAction } from './types';

export const defaultState: SideBarState = {
  display: false,
};

function topPostsReducer(
  state: SideBarState = defaultState,
  action: SideBarAction,
): SideBarState {
  switch (action.type) {
    case constants.CLOSE_SIDE_BAR: {
      return { display: false };
    }

    case constants.OPEN_SIDE_BAR: {
      return { display: true };
    }

    default:
      return state;
  }
}

export default topPostsReducer;
