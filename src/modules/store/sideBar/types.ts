import constants from './constants';

export type SideBarState = {
  display: boolean;
};

export type OpenSideBarAction = {
  type: constants.OPEN_SIDE_BAR;
};

export type CloseSideBarAction = {
  type: constants.CLOSE_SIDE_BAR;
};

export type SideBarAction = OpenSideBarAction | CloseSideBarAction;
