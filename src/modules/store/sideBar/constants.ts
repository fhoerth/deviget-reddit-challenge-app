enum Constants {
  OPEN_SIDE_BAR = '@@sideBar/OPEN_SIDE_BAR',
  CLOSE_SIDE_BAR = '@@sideBar/CLOSE_SIDE_BAR',
}

export default Constants;
