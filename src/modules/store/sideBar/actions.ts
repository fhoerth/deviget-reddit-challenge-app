import {
  OpenSideBarAction,
  CloseSideBarAction,
} from './types';

import constants from './constants';

export const openSideBar = (): OpenSideBarAction => ({
  type: constants.OPEN_SIDE_BAR,
});

export const closeSideBar = (): CloseSideBarAction => ({
  type: constants.CLOSE_SIDE_BAR,
});
