import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';
import {
  applyMiddleware,
  combineReducers,
  createStore as createReduxStore,
  Store,
} from 'redux';

import postReducer from './post/reducer';
import sideBarReducer from './sideBar/reducer';
import topPostsReducer from './topPosts/reducer';

import createRootSaga from './rootSaga';
import { RootState, RootAction } from './types';

function createStore(): Store<RootState, RootAction> {
  const sagaMiddleware = createSagaMiddleware();
  const store = createReduxStore<RootState, RootAction, unknown, unknown>(
    combineReducers({
      post: postReducer,
      sideBar: sideBarReducer,
      topPosts: topPostsReducer,
    }),
    composeWithDevTools(applyMiddleware(
      sagaMiddleware,
    )),
  );

  const rootSaga = createRootSaga();
  sagaMiddleware.run(rootSaga);

  return store;
}

export default createStore;
