import { Post } from '../../reddit/types';

import constants from './constants';

export type PostState = {
  fetchingPost: boolean;
  fetchPostRejected: boolean;
  fetchPostFulfilled: boolean;
  id: string | null;
  data: Post | null;
};

export type FetchPostAction = {
  type: constants.FETCH_POST;
  payload: {
    id: string;
  };
};

export type FetchPostRejectedAction = {
  type: constants.FETCH_POST_REJECTED;
};

export type FetchPostFulfilledAction = {
  type: constants.FETCH_POST_FULFILLED;
  payload: {
    post: Post
  };
};

export type PostAction = FetchPostAction
  | FetchPostRejectedAction
  | FetchPostFulfilledAction;
