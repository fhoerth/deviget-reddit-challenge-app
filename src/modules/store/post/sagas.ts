import { SagaIterator } from 'redux-saga';
import { call, put, takeEvery } from 'redux-saga/effects';

import * as apiClient from '../../reddit/apiClient';
import { Post } from '../../reddit/types';

import * as actions from './actions';
import constants from './constants';
import { FetchPostAction } from './types';

export function* fetchPostSaga(action: FetchPostAction): SagaIterator<void> {
  const { id } = action.payload;

  try {
    const post: Post = yield call(apiClient.fetchPost, id);

    yield put(actions.fetchPostFulfilled({ post }));
  } catch (e) {
    yield put(actions.fetchPostRejected());
  }
}

export function* watchFetchPostSaga(): SagaIterator<void> {
  yield takeEvery(constants.FETCH_POST, fetchPostSaga);
}
