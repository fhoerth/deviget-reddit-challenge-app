import {
  FetchPostAction,
  FetchPostRejectedAction,
  FetchPostFulfilledAction,
} from './types';
import constants from './constants';

export const fetchPost = (payload: FetchPostAction['payload']): FetchPostAction => ({
  payload,
  type: constants.FETCH_POST,
});

export const fetchPostRejected = (): FetchPostRejectedAction => ({
  type: constants.FETCH_POST_REJECTED,
});

export const fetchPostFulfilled = (payload: FetchPostFulfilledAction['payload']): FetchPostFulfilledAction => ({
  payload,
  type: constants.FETCH_POST_FULFILLED,
});
