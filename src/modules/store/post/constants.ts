enum Constants {
  FETCH_POST = '@@post/FETCH_POST',
  FETCH_POST_FULFILLED = '@@post/FETCH_POST_FULFILLED',
  FETCH_POST_REJECTED = '@@post/FETCH_POST_REJECTED',
}

export default Constants;
