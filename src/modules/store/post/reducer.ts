import constants from './constants';
import { PostAction, PostState } from './types';

export const defaultState: PostState = {
  fetchPostFulfilled: false,
  fetchPostRejected: false,
  fetchingPost: false,
  id: null,
  data: null,
};

function postReducer(
  state: PostState = defaultState,
  action: PostAction,
): PostState {
  switch (action.type) {
    case constants.FETCH_POST: {
      const { payload } = action;
      const { id } = payload;

      return {
        ...state,
        id,
        fetchingPost: true,
        fetchPostFulfilled: false,
        fetchPostRejected: false,
      };
    }

    case constants.FETCH_POST_FULFILLED: {
      const { payload } = action;
      const { post } = payload;

      return {
        ...defaultState,
        data: post,
        fetchPostFulfilled: true,
        fetchPostRejected: false,
        fetchingPost: false,
      };
    }

    case constants.FETCH_POST_REJECTED: {
      return {
        ...defaultState,
        fetchPostRejected: true,
        fetchPostFulfilled: false,
        fetchingPost: false,
      };
    }

    default:
      return state;
  }
}

export default postReducer;
