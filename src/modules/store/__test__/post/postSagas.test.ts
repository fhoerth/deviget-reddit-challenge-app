import { runSaga } from 'redux-saga';
import { takeEvery } from 'redux-saga/effects';

import * as apiClient from '../../../reddit/apiClient';
import { createPostMock } from '../../../common/testUtils';

import * as actions from '../../post/actions';
import constants from '../../post/constants';
import { PostAction } from '../../post/types';
import { fetchPostSaga, watchFetchPostSaga } from '../../post/sagas';


describe('postSagas', () => {
  it('waits for FETCH_POST', () => {
    const generator = watchFetchPostSaga();

    expect(generator.next().value).toEqual(takeEvery(constants.FETCH_POST, fetchPostSaga));
    expect(generator.next().done).toBe(true);
  });

  describe('when FETCH_POST action is dispatched', () => {
    describe('and fetch is successfull', () => {
      it('dispatches a fulfilled action', async () => {
        const post = createPostMock();
        const action = actions.fetchPost({ id: post.id });

        const spy = jest.spyOn(apiClient, 'fetchPost').mockImplementationOnce(() => Promise.resolve(post));

        let dispatched: PostAction[] = [];

        await runSaga(
          {
            dispatch: (postAction: PostAction): void => {
              dispatched = [...dispatched, postAction];
            },
          },
          fetchPostSaga,
          action,
        ).toPromise();

        spy.mockClear();

        expect(dispatched).toEqual([
          actions.fetchPostFulfilled({ post }),
        ]);
      });
    });

    describe('and fetch is unsuccessfull', () => {
      it('dispatches a rejected action', async () => {
        const error = new Error('Oops!');
        const post = createPostMock();
        const action = actions.fetchPost({ id: post.id });

        const spy = jest.spyOn(apiClient, 'fetchPost').mockImplementationOnce(() => Promise.reject(error));

        let dispatched: PostAction[] = [];

        await runSaga(
          {
            dispatch: (postAction: PostAction): void => {
              dispatched = [...dispatched, postAction];
            },
          },
          fetchPostSaga,
          action,
        ).toPromise();

        spy.mockClear();

        expect(dispatched).toEqual([
          actions.fetchPostRejected(),
        ]);
      });
    });
  });
});
