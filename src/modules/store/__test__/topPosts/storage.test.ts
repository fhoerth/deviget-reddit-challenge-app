import * as storage from '../../topPosts/storage';

describe('Storage', () => {
  it('stores and retrieves data', () => {
    const visited = ['a', 'b', 'c'];
    const dismissed = ['1', '2', '3'];
    const allDismissed = false;

    storage.clear();

    expect(storage.get()).toBe(null);
    storage.set({ visited, dismissed, allDismissed });

    expect(storage.get()).toEqual({ visited, dismissed, allDismissed });

    storage.clear();
  });
});
