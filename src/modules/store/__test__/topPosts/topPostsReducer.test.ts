import { createTopPostsMock } from '../../../common/testUtils';

import { Post } from '../../../reddit/types';

import reduce, { defaultState } from '../../topPosts/reducer';
import { TopPostsState } from '../../topPosts/types';
import * as actions from '../../topPosts/actions';

describe('DISMISS_TOP_POSTS', () => {
  it('dismisses a post', () => {
    const posts = createTopPostsMock(50);
    const postToBeDismissed = posts[0];

    const state = {
      ...defaultState,
      data: posts,
    };

    const result = reduce(state, actions.dismissTopPost({ post: postToBeDismissed }));

    expect(result).toMatchObject({
      dismissed: [posts[0].id],
    });
  });

  describe('when all posts are dismissed', () => {
    it('sets allDismissed to true', () => {
      const posts = createTopPostsMock(50);
      const initialState = {
        ...defaultState,
        data: posts,
      };

      const result = posts.reduce((state: TopPostsState, post: Post): TopPostsState => (
        reduce(state, actions.dismissTopPost({ post }))
      ), initialState);

      expect(result).toMatchObject({
        allDismissed: true,
        dismissed: posts.map(post => post.id),
      });
    });
  });
});
