import { runSaga } from 'redux-saga';
import { takeEvery } from 'redux-saga/effects';

import config from '../../../reddit/config';
import { createTopPostsMock } from '../../../common/testUtils';
import * as apiClient from '../../../reddit/apiClient';

import * as actions from '../../topPosts/actions';
import constants from '../../topPosts/constants';
import { defaultState } from '../../topPosts/reducer';
import { TopPostsAction, FetchTopPostsFulfilledAction } from '../../topPosts/types';
import {
  fetchTopPostsSaga,
  watchFetchTopPostsSaga,
} from '../../topPosts/sagas';

import { RootState } from '../../types';

describe('postSagas', () => {
  it('waits for FETCH_TOP_POSTS', () => {
    const generator = watchFetchTopPostsSaga();

    expect(generator.next().value).toEqual(takeEvery(constants.FETCH_TOP_POSTS, fetchTopPostsSaga));
    expect(generator.next().done).toBe(true);
  });

  describe('fetchFirstNonDismissedTopPostsSaga', () => {
    it('resolves first top posts by excluding dismissed posts', async () => {
      /**
       * In this test we will simulate the situation where in a certain fetch,
       * some posts have been dismissed.
       * Next page should be fetched in order
       * to be able to resolve a list with desired amount of items.
       */
      const firstPosts = createTopPostsMock(config.topPosts.limit);
      const secondPosts = createTopPostsMock(config.topPosts.limit);
      const spy = jest.spyOn(apiClient, 'fetchTopPosts')
        .mockImplementationOnce(() => Promise.resolve(firstPosts))
        .mockImplementationOnce(() => Promise.resolve(secondPosts));

      // Create a list of dismissed posts.
      const dismissed = firstPosts.slice(25, config.topPosts.limit).map(post => post.id);

      let dispatched: FetchTopPostsFulfilledAction[] = [];
      await runSaga(
        {
          getState: (): Partial<RootState> => ({
            topPosts: {
              ...defaultState,
              dismissed,
            },
          }),
          dispatch: (action: FetchTopPostsFulfilledAction): void => {
            dispatched = [...dispatched, action];
          },
        },
        fetchTopPostsSaga,
      ).toPromise();

      spy.mockClear();

      const [fetchTopPostsFulfilled] = dispatched;
      const expectedPosts = firstPosts.slice(0, 25).concat(secondPosts.slice(0, 25));

      expect(fetchTopPostsFulfilled.payload.posts).toEqual(expectedPosts);
    });

    it('resolves from next page when all posts from current page are dismissed', async () => {
      const firstPosts = createTopPostsMock(config.topPosts.limit);
      const secondPosts = createTopPostsMock(config.topPosts.limit);
      const spy = jest.spyOn(apiClient, 'fetchTopPosts')
        .mockImplementationOnce(() => Promise.resolve(firstPosts))
        .mockImplementationOnce(() => Promise.resolve(secondPosts));

      const dismissed = firstPosts.map(post => post.id);

      let dispatched: FetchTopPostsFulfilledAction[] = [];
      await runSaga(
        {
          getState: (): Partial<RootState> => ({
            topPosts: {
              ...defaultState,
              dismissed,
            },
          }),
          dispatch: (action: FetchTopPostsFulfilledAction): void => {
            dispatched = [...dispatched, action];
          },
        },
        fetchTopPostsSaga,
      ).toPromise();

      spy.mockClear();

      const [fetchTopPostsFulfilled] = dispatched;

      expect(fetchTopPostsFulfilled.payload.posts).toEqual(secondPosts);
    });

    it('resolves an empty array when all posts are dismissed', async () => {
      const firstPosts = createTopPostsMock(config.topPosts.limit);
      const secondPosts = createTopPostsMock(config.topPosts.limit);
      const spy = jest.spyOn(apiClient, 'fetchTopPosts')
        .mockImplementationOnce(() => Promise.resolve(firstPosts))
        .mockImplementationOnce(() => Promise.resolve(secondPosts))
        .mockImplementationOnce(() => Promise.resolve([]));

      const dismissed = [
        ...firstPosts.map(post => post.id),
        ...secondPosts.map(post => post.id),
      ];

      let dispatched: FetchTopPostsFulfilledAction[] = [];
      await runSaga(
        {
          getState: (): Partial<RootState> => ({
            topPosts: {
              ...defaultState,
              dismissed,
            },
          }),
          dispatch: (action: FetchTopPostsFulfilledAction): void => {
            dispatched = [...dispatched, action];
          },
        },
        fetchTopPostsSaga,
      ).toPromise();

      spy.mockClear();

      const [fetchTopPostsFulfilled] = dispatched;

      expect(fetchTopPostsFulfilled.payload.posts.length).toBe(0);
    });
  });

  describe('when FETCH_TOP_POSTS action is dispatched', () => {
    describe('and fetch is successfull', () => {
      it('dispatches a fulfilled action', async () => {
        const posts = createTopPostsMock(config.topPosts.limit);
        const spy = jest.spyOn(apiClient, 'fetchTopPosts').mockImplementationOnce(() => Promise.resolve(posts));

        let dispatched: TopPostsAction[] = [];

        await runSaga(
          {
            getState: (): Partial<RootState> => ({
              topPosts: defaultState,
            }),
            dispatch: (action: TopPostsAction): void => {
              dispatched = [...dispatched, action];
            },
          },
          fetchTopPostsSaga,
        ).toPromise();

        spy.mockClear();

        expect(dispatched).toEqual([
          actions.fetchTopPostsFulfilled({ posts }),
        ]);
      });
    });

    describe('and fetch is unsuccessfull', () => {
      it('dispatches a rejected action', async () => {
        const error = new Error('Oops!');
        const spy = jest.spyOn(apiClient, 'fetchTopPosts').mockImplementationOnce(() => Promise.reject(error));

        let dispatched: TopPostsAction[] = [];

        await runSaga(
          {
            dispatch: (action: TopPostsAction): void => {
              dispatched = [...dispatched, action];
            },
          },
          fetchTopPostsSaga,
        ).toPromise();

        spy.mockClear();

        expect(dispatched).toEqual([
          actions.fetchTopPostsRejected(),
        ]);
      });
    });
  });
});
