import { Post, PostImage } from '../reddit/types';

import { VisitedPosts, DismissedPosts } from './types';

export const getPostImageUrl = (postImage: PostImage): string => (
  postImage.url.replace(/&amp;/g, '&')
);

export const addVisitedPost = (
  visitedPosts: VisitedPosts,
  post: Post,
): VisitedPosts => visitedPosts.concat(post.id);

export const isPostVisited = (
  visitedPosts: VisitedPosts,
  post: Post,
): boolean => !!visitedPosts.find(visitedPostId => visitedPostId === post.id);

export const addDismissedPost = (
  dismissedPosts: DismissedPosts,
  post: Post,
): DismissedPosts => dismissedPosts.concat(post.id);

export const addDismissedPosts = (
  dismissedPosts: DismissedPosts,
  posts: Array<Post>,
): DismissedPosts => dismissedPosts.concat(posts.map(post => post.id));

export const isPostDismissed = (
  dismissedPosts: DismissedPosts,
  post: Post,
): boolean => !!dismissedPosts.find(dismissedPostId => dismissedPostId === post.id);

export const excludePost = (
  posts: Array<Post>,
  post: Post,
): Array<Post> => posts.filter(postItem => postItem.id !== post.id);

export const excludeDismissedPosts = (
  posts: Array<Post>,
  dismissedPosts: DismissedPosts,
): Array<Post> => {
  const reducer = (
    keys: Record<string, string>,
    postId: string,
  ): Record<string, string> => ({ ...keys, [postId]: postId });

  const keys = dismissedPosts.reduce(reducer, {});

  return posts.filter(post => !keys[post.id]);
};

export const unescapeHtml = (
  escapedHtml: string,
): string | null => {
  const parser = new DOMParser();
  const content = parser.parseFromString(escapedHtml, 'text/html');

  return content.documentElement.textContent;
};
