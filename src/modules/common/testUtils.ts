import faker from 'faker';

import { Post } from '../reddit/types';

export const createPostMock = (): Post => ({
  id: faker.random.uuid(),
  name: `t3_${faker.random.uuid()}`,
  author: faker.internet.userName(),
  author_fullname: faker.internet.userName(),
  title: faker.lorem.sentence(),
  subreddit_name_prefixed: `/r${faker.company.companyName()}`,
  thumbnail: faker.image.imageUrl(),
  thumbnail_width: faker.random.number(),
  thumbnail_height: faker.random.number(),
  selftext_html: faker.lorem.paragraph(),
  num_comments: 23,
  created_utc: 31234234,
  permalink: 'string',
  url: faker.internet.url(),
  preview: {
    images: [],
  },
});

export const createTopPostsMock = (quantity: number): Array<Post> => {
  const posts: Array<Post> = new Array(quantity).fill(null).map(() => createPostMock());

  return posts;
};

export default createPostMock;
