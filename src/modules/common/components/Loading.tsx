import React from 'react';
import classnames from 'classnames';

import styles from './styles/Loading.scss';

type LoadingProps = {
  theme: 'dark' | 'light';
};

const Loading: React.SFC<LoadingProps> = ({ theme }: LoadingProps) => (
  <div
    className={classnames(
      styles.spinner,
      theme === 'dark' ? styles.spinnerDark : styles.spinnerLight,
    )}
  >
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
    <div />
  </div>
);

export default Loading;
