import React from 'react';

import styles from './styles/Error.scss';

type ErrorProps = {
  message: string;
};

const Error: React.SFC<ErrorProps> = ({ message }: ErrorProps) => (
  <div className={styles.error}>{message}</div>
);

export default Error;
