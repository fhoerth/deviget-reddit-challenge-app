export type VisitedPosts = Array<string>;
export type DismissedPosts = Array<string>;
