# Deviget Reddit Challenge Application

[Open on Heroku](https://deviget-reddit-challenge-app.herokuapp.com/)

### Installation
```console
npm i
```
###  Running in development
```console
npm run dev
```
The server starts at `http://localhost:5000`

###  Running in production
```console
npm run build && npm run start
```
The server starts at `http://localhost:5000`

### Running tests
```console
npm run test
```

### Requirements to run the application.
```
NodeJS >= 8
```

## General notes
I tried to keep the application with less dependencies as possible.

## About development process.
I started with a very basic layout, followed by `reddit` module, then `store` and finally finish the UI.

## About Redux.
I found myself a little stuck, specially because I haven't been working with redux in the last 2years+ (specially with hooks + typescript).
Documentation doesn't provide a "good" way to do things.
For example: Seems that fetching initial data in a `useEffect` hook is the best approach, but `useLayoutEffect` could be used too.
I just followed React recomendations about trying to avoid `useLayoutEffect` if `useEffect` works, that's why I finally used `useEffect`.
Also isn't very clear what's best: if calling `useSelector` hook once per field, or retrieve multiple fields using `shallowEqual`.


## About responsivness.
I accomplished all the requirments using plain css (actually scss).
This isn't clearly the best approach. In order to make the `SideBar` responsive, I had
to render a `SideBar` for `small` resolutions, and another `SideBar` for `medium` and `large` resolutions.
Using `display: none` to remove elements doesn't prevent React from mounting components.
If the component fetches data, data is fetched twice.
Creating a context and setting states based on current resolution will help to prevent rendering components that shouldn't be rendered (by setting `resize` listener on the window object).

## About persisting the store.
I chose to use sagas to persist store data. But alternativelly a middleware could be implemented.

## About react-router.
This app could be done completely without a router. I just think that an app like reddit should allow users to access specific posts.

## What I added?
Once posts are dismissed, the next time the user enters the application it will recursevily fetch posts
until it reachs 50 posts. This way the user is able to get the list of the top 50 non-dismissed posts.
In the case that all posts are dismissed, the user has the posibility to press a button to fetch the next top 50 posts.
This feature of course brings with it great performance issues, since the more dismissed posts the more api calls (reddit api itself is kind of slow, so more than 2-3 fetches to complete the list makes the app almose unusable).
I think this feature is very challenging, and there isn't much that can be done in order to decrease the performance issue, specially on the client side.

## What I would add?
Some unit tests. Specially in components. Jest + Enzyme is already configured, but due to lack of time
I couldn't add more tests.

## What stack I used.
* webpack.
* sass + css-modules.
* TypeScript.
* eslint typescript configuration
* jest for unit tests.
* react
* redux
* react-router
* redux-saga for handling async side effects.

## Why I coded the application in TypeScript.
I think typescript adds a little overhead, specially using `redux` and `redux-saga`, but on the other hand it enhances the code quality by giving more readability, making it easier to follow. Since my code is going to be reviewed, I want other developers to understand what I try to do in every line of code as much as possible.
