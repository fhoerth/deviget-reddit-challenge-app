const path = require('path');
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const env = require('./env');
const config = require('./config');

const staticRelativePath = config.paths.static.replace(/^\//, '');
const file = filename => `${staticRelativePath}/${filename}`;

const devPlugins = [
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NoEmitOnErrorsPlugin(),
];
const prodPlugins = [
  new OptimizeCSSAssetsPlugin({}),
];

const mode = env.production ? 'production' : 'development';
const devtool = env.production ? false : 'source-maps';
const plugins = [
  ...(env.production ? prodPlugins : devPlugins),
  new MiniCssExtractPlugin({
    filename: file('[name].[contenthash].css'),
    chunkFilename: file('[id].[contenthash].css'),
  }),
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
  }),
  new FriendlyErrorsWebpackPlugin(),
  new HtmlWebpackPlugin(),
  new CleanWebpackPlugin(),
];
const rules = {
  tsx: {
    test: /\.(tsx?)$/,
    enforce: 'pre',
    exclude: /node_modules/,
    use: [
      // env.development ? 'react-hot-loader/webpack' : null,
      'babel-loader',
      'ts-loader',
    ].filter(Boolean),
  },
  scss: {
    test: /\.scss$/,
    use: [env.production ? MiniCssExtractPlugin.loader : 'style-loader']
      .concat({
        loader: 'css-modules-typescript-loader',
        options: {
          mode: env.production ? 'verify' : 'emit',
        },
      })
      .concat({
        loader: 'css-loader',
        options: {
          modules: {
            mode: 'local',
            exportGlobals: true,
            context: path.resolve(config.directories.input),
            localIdentName: '[name]__[local]___[hash:base64:5]',
          },
          localsConvention: 'camelCase',
          importLoaders: true,
          sourceMap: true,
        },
      })
      .concat({
        loader: 'sass-loader',
        options: {
          sourceMap: true,
        },
      }),
  },
};

module.exports = {
  mode,
  devtool,
  plugins,
  devServer: {
    hot: true,
    writeToDisk: true,
    historyApiFallback: true,
    port: env.port,
    contentBase: config.directories.output,
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.scss', '.css'],
    ...(env.development ? {
      alias: {
        'react-dom': '@hot-loader/react-dom',
      },
    } : null),
  },
  entry: {
    main: (env.development ? [
      'react-hot-loader/patch',
      `webpack-dev-server/client?http://0.0.0.0:${env.port}`,
      'webpack/hot/only-dev-server',
    ] : []).concat([
      path.join(config.directories.input, 'entry.tsx'),
    ]),
    vendor: [
      'react',
      'react-dom',
    ],
  },
  output: {
    path: config.directories.output,
    publicPath: '/',
    filename: env.production ? file('[name].[contenthash].js') : file('[name].js'),
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          chunks: 'initial',
          name: 'vendor',
          test: 'vendor',
          enforce: true,
        },
      },
    },
  },
  module: {
    rules: [
      rules.tsx,
      rules.scss,
    ],
  },
};