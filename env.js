const process = require('process');

const production = !!(process.env.NODE_ENV && process.env.NODE_ENV === 'production');
const development = !production;

module.exports = {
  production,
  development,
  port: process.env.PORT || 5000,
};