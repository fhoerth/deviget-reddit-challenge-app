module.exports = {
  globals: { document: true, DOMParser: true },
  plugins: ['jest'],
  env: {
    jest: true,
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  overrides: [
    {
      files: ['*.ts', '*.tsx'],
      parser: '@typescript-eslint/parser',
      extends: ['plugin:@typescript-eslint/recommended', 'airbnb'],
      rules: {
        'react/jsx-filename-extension': [2, { extensions: ['.tsx'] }],
        'arrow-parens': [0],
        'import/extensions': [
          'error',
          'ignorePackages',
          { ts: 'never', tsx: 'never' },
        ],
        'jsx-a11y/click-events-have-key-events': 0,
        'jsx-a11y/no-static-element-interactions': 0,
        'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
      },
      settings: {
        'import/resolver': {
          node: { extensions: ['.js', '.jsx', '.ts', '.tsx'] },
        },
      },
    },
  ],
};