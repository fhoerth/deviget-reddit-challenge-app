const path = require('path');

module.exports = {
  directories: {
    input: path.join(__dirname, 'src'),
    output: path.join(__dirname, 'dist'),
  },
  paths: {
    static: '/static',
  },
};