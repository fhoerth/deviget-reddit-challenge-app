const babelConfig = require('./babel.config');

module.exports = {
  preset: 'ts-jest',
  moduleFileExtensions: ['js', 'ts', 'tsx'],
  globals: {
    'ts-jest': { babelConfig },
  },
  setupFilesAfterEnv: ['<rootDir>/jest.setup.js'],
  moduleNameMapper: {
    '\\.(scss)$': 'identity-obj-proxy',
  },
};